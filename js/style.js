/*
  1. Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?

     Когда функция решает задачу, в процессе она может вызвать многие другие функцыи

     Частичный случай этого заключается в том, когда функция вызывает сама себя
     
                         Это называется рекурсия
*/


const number = +prompt("Enter your number");

 const numberFactorial = (number) => {
    return (number !== 1) ? number * numberFactorial(number - 1) : 1;
}

console.log(numberFactorial(number))
